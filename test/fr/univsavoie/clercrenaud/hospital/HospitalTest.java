package fr.univsavoie.clercrenaud.hospital;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr.univsavoie.clercrenaud.hospital.Hospital;

public class HospitalTest {

    private Hospital hopital;

    @Before
    public void setUp(){
        hopital = new Hospital("FBDDDBT");
    }

    @Test
    public void testHeal() throws Exception {
        assertEquals("F1-B2-D0-T1-M3", hopital.heal());
    }


    @Test
    public void testAspirine() throws Exception {
        hopital.aspirine();
        assertEquals("F0-B3-D0-T1-M3", hopital.heal());
    }

    @Test
    public void testAntibiotic() throws Exception {
        hopital.antibiotic();
        assertEquals("F1-B3-D0-T0-M3", hopital.heal());
    }

    @Test
    public void testInsuline() throws Exception {
        hopital.insuline();
        assertEquals("F1-B2-D3-T1-M0", hopital.heal());
    }

    @Test
    public void antibioticPlusInsuline() throws Exception {
        hopital.antibiotic();
        hopital.insuline();
        assertEquals("F3-B1-D3-T0-M0", hopital.heal());
    }


}
