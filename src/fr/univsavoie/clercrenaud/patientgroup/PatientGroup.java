package fr.univsavoie.clercrenaud.patientgroup;

import java.util.ArrayList;

import fr.univsavoie.clercrenaud.hospital.Patient;

public abstract class PatientGroup {
	
	protected ArrayList<Patient> patients;
	
	public PatientGroup() {
		patients = new ArrayList<Patient>();
	}
	
	public void addToGroup(Patient newPatient) {
		patients.add(newPatient);
	}
	
	public ArrayList<Patient> getPatients() {
		return patients;
	}
	
	public void transferTo(PatientGroup patientGroup) {
		patientGroup.getPatients().addAll(patients);
		patients.clear();
	}
}
