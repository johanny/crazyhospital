package fr.univsavoie.clercrenaud.patientgroup;

public class DeadPatientGroup extends PatientGroup {

	@Override
	public String toString() {
		return "M" + patients.size();
	}

}
