package fr.univsavoie.clercrenaud.patientgroup;

public class HealthyPatientGroup extends PatientGroup {

	@Override
	public String toString() {
		return "B" + patients.size();
	}
	
}
