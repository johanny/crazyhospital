package fr.univsavoie.clercrenaud.patientgroup;

public class DiabetePatientGroup extends PatientGroup {

	@Override
	public String toString() {
		return "D" + patients.size();
	}

}
