package fr.univsavoie.clercrenaud.patientgroup;

public class FeverPatientGroup extends PatientGroup {

	@Override
	public String toString() {
		return "F" + patients.size();
	}
	
}
