package fr.univsavoie.clercrenaud.hospital;

import java.util.ArrayList;

import fr.univsavoie.clercrenaud.patientgroup.*;

public class Hospital {
	
	private FeverPatientGroup feverPatientGroup;
	private HealthyPatientGroup healthyPatientGroup;
	private DiabetePatientGroup diabetePatientGroup;
	private TubercularPatientGroup tubercularPatientGroup;
	private DeadPatientGroup deadPatientGroup;
	
	private PatientStringParser patientStringParser;
	
	public Hospital(String sickPeople) {
		patientStringParser = new PatientStringParser();
		
		feverPatientGroup = (FeverPatientGroup) patientStringParser.parse(sickPeople, 'F');
		healthyPatientGroup = (HealthyPatientGroup) patientStringParser.parse(sickPeople, 'B');
		diabetePatientGroup = (DiabetePatientGroup) patientStringParser.parse(sickPeople, 'D');
		tubercularPatientGroup = (TubercularPatientGroup) patientStringParser.parse(sickPeople, 'T');
		deadPatientGroup = new DeadPatientGroup();
	}
	
    public void aspirine() {
        feverPatientGroup.transferTo(healthyPatientGroup);
    }

    public void antibiotic() {
        tubercularPatientGroup.transferTo(healthyPatientGroup);
    }

    public void insuline() {
        throw new UnsupportedOperationException();
    }

    public String heal() {
    	oneDayPass();
    	
    	String checkUp = feverPatientGroup.toString() + "-"
    					 + healthyPatientGroup.toString() + "-"
    					 + diabetePatientGroup.toString() + "-"
    					 + tubercularPatientGroup.toString() + "-"
    					 + deadPatientGroup.toString();
    	return checkUp;
    }
    
    private void oneDayPass() {
    	diabetePatientGroup.transferTo(deadPatientGroup);
    }

}
