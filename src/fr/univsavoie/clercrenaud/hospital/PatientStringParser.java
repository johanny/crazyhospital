package fr.univsavoie.clercrenaud.hospital;

import java.util.ArrayList;

import fr.univsavoie.clercrenaud.patientgroup.*;

public class PatientStringParser {
	
	public PatientStringParser() {
		
	}
	
	public PatientGroup parse(String patientString, char patientKind) {
		PatientGroup patientGroup = definePatientGroup(patientKind);
		
		for( int i = 0; i < patientString.length(); i++ ) {
			char patientCharacter = patientString.charAt(i);
			
			if(patientCharacter == patientKind) {
				patientGroup.addToGroup(new Patient());
			}
		}
		
		return patientGroup;
	}
	
	private PatientGroup definePatientGroup(char patientKind) {
		PatientGroup patientGroup = null;
		
		if(patientKind == 'F') {
			patientGroup = new FeverPatientGroup();
		} 
		else if (patientKind == 'B') {
			patientGroup = new HealthyPatientGroup();
		}
		else if (patientKind == 'D') {
			patientGroup = new DiabetePatientGroup();
		}
		else if (patientKind == 'T') {
			patientGroup = new TubercularPatientGroup();
		}
		
		return patientGroup;
	}

}
