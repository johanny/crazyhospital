package fr.univsavoie.clercrenaud;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.univsavoie.clercrenaud.hospital.Hospital;

public class CrazyHospitalLauncherTest {

	@Test
	public void creationTest() {
		Hospital hospital = new Hospital("FBDDDBT");
		assertEquals("F:1 B:2 D:0 T:1 M:3", hospital.heal());
	}

}
